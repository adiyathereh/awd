const { createTestClient } = require("apollo-server-testing");
const gql = require("graphql-tag");
const { ApolloServer } = require("apollo-server-express");

const typeDefs = require("../typeDefs");
const resolvers = require("../resolvers");

const dbMock = [{ title: "test item", index: 1, array: [], id: "abc1" }];

it("get all items", async () => {
  resolvers.Query.getAllItems = () => dbMock;
  // use the test server to create a query function
  const apolloTestServer = new ApolloServer({ typeDefs, resolvers });

  const { query } = createTestClient(apolloTestServer);

  // run query against the server and snapshot the output
  const res = await query({
    query: gql`
      query {
        getAllItems {
          title
          index
          id
          array
        }
      }
    `
  });

  const resData = ({ getAllItems } = await res.data.getAllItems);

  expect(resData).toEqual([({ title, index, id } = dbMock[0])]);
});
