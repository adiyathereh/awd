const item = require("../schema/item");

module.exports = {
  getAllItems: () => item.find(),
  getItem: id => item.findById({ _id: id }),
  createItem: ({ title, index, array }) =>
    new item({ title, index, array }).save()
};
