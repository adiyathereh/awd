require("dotenv").config();

const express = require("express");
const mongoose = require("mongoose");

const createApolloServer = require("./createApolloServer");

const server = createApolloServer();
const app = express();

server.applyMiddleware({ app, bodyParser: true });

app.get("/", (req, res) => res.send("Hello world!"));

mongoose.connect(
  process.env.DB_URI,
  { useNewUrlParser: true },
  async () => {
    console.log("db connected");
  }
);

app.listen((port = process.env.PORT), () => {
  console.log(`😬 Server started on port ${port}`);
  console.log(`🚀 Graphiql at http://localhost:${port + server.graphqlPath}`);
});
