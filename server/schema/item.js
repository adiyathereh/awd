const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const itemSchema = new Schema({
  title: String,
  index: Number,
  array: Array
});

module.exports = mongoose.model("ITEM", itemSchema);
