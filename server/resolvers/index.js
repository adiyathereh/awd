const { getAllItems, getItem, createItem } = require("../db_connector");

module.exports = {
  Query: {
    getAllItems,
    getItem: (root, { id }) => getItem(id)
  },

  Mutation: {
    createItem: (root, { title, index = 2, array = [] }, context) =>
      createItem({ title, index, array })
  }
};
