const { gql } = require("apollo-server-express");

// Construct a schema, using GraphQL schema language
module.exports = gql`
  type Query {
    getAllItems: [Item]
    getItem(id: String): Item
  }

  type Item {
    id: ID
    title: String
    index: Int
    array: [String]
  }

  type Mutation {
    createItem(title: String, index: Int, array: [String]): Item
  }
`;
