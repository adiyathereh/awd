# Boilerplate for server setup with Express, MongoDB, Apollo Server.

Initial setup requires the creation of an .env file with two environment variables: DB_URI and PORT.

Examples:
```
DB_URI=mongodb://localhost:27017/NAME_OF_YOUR_COLLECTION
PORT=3000
```

*localhost:27017 is the default port for MongoDB
 
To start the server:
```
yarn start
```
I used nodemon so that there is not need for reload every time there is a change

To test queries and mutations access graphql playground from the link that is console.logged on starting the server or type in the browser:
```
http://localhost:PORT_FROM_ENV_FILE/graphql
```