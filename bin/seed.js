const mongoose = require("mongoose");
const awd = require("./server/schema/awd");

mongoose.connect(
  "mongodb://localhost:27017/awd",
  { useNewUrlParser: true },
  async () => {
    const newAwd = {
      title: "hello world!",
      index: 1,
      array: ["bla", "foo", "bar"]
    };
    await new awd(newAwd).save().then(console.log);

    await mongoose.disconnect();
  }
);
